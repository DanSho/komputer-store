const bankAmountElement = document.getElementById("balance");

const loanAmountElement = document.getElementById("outstandingLoan");

const salaryAmountElement = document.getElementById("salary");

const laptopsElement = document.getElementById("laptops");

const bankBtnElement = document.getElementById("bank_btn");

const descriptionElement = document.getElementById("description");
const titleElement = document.getElementById("computerName");

const workBtnElement = document.getElementById("work_btn");
const buyBtnElement = document.getElementById("buy_btn");
const getLoanBtnElement = document.getElementById("getLoan_btn");

const repayLoanBtnElement = document.getElementById("repay_btn");
const priceElement = document.getElementById("price");
const specsElement = document.getElementById("specs");

let balance = 0;

let salary = 0;

let laptops = [];

let outstandingLoan = 0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToInfo(laptops));

workBtnElement.addEventListener("click", () => {
  salary += 100;
  salaryAmountElement.innerHTML = salary + " kr";
});

const addLaptopsToInfo = (laptops) => {
  laptops.forEach((x) => addLaptopToInfo(x));
  priceElement.innerText = laptops[0].price;
  titleElement.innerHTML = laptops[0].title;
  descriptionElement.innerHTML = laptops[0].description;
  const lapImg = document.getElementById("img");
  lapImg.setAttribute(
    "src",
    `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`
  );
};

const addLaptopToInfo = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title)); //done
  laptopsElement.appendChild(laptopElement);
};

const chosenLaptopFunction = (e) => {
  const choseLaptop = laptops[e.target.selectedIndex];
  titleElement.innerHTML = choseLaptop.title;
  priceElement.innerText = choseLaptop.price;
  descriptionElement.innerHTML = choseLaptop.description;

  const lapImg = document.getElementById("img");

  lapImg.setAttribute(
    "src",
    `https://noroff-komputer-store-api.herokuapp.com/${choseLaptop.image}`
  );

  titleElement.innerText = laptops.title;
};

//Basically it adds salary to balance.
//10% of your income added to loans brings your total to 90%
//If 10% of pay is more than the outstanding loan, the remaining amount will be added to the debt.

bankBtnElement.addEventListener("click", () => {
  let deductionAmount = salary * 0.1;
  let myAmount = salary * 0.9;

  if (outstandingLoan == 0) {
    balance += salary;
    bankAmountElement.innerHTML = balance + "kr";
    salary = 0;
    salaryAmountElement.innerText = salary + " kr";
  }
  if (salary > outstandingLoan) {
    let rest = deductionAmount - outstandingLoan;
    balance += myAmount + rest;
    outstandingLoan -= outstandingLoan;
    salary = 0;
    salaryAmountElement.innerText = salary;
    bankAmountElement.innerHTML = balance + "kr";
    loanAmountElement.innerHTML = outstandingLoan;
  }
  if (outstandingLoan > salary) {
    balance += myAmount;
    bankAmountElement.innerHTML = balance + "kr";
    salary = 0;
    salaryAmountElement.innerText = salary + " kr";
    outstandingLoan -= deductionAmount;
  }
});

//  if outstanding Loan is more than 0 then you cannot another loan
// Asks the user to enter an amount, then checks if the requested amount the user entered is twice than the balance
// if not then outstanding loan is added to the requested loan and then the balance is also added to the outstanding loan then the balance and outstanding loan is updated

getLoanBtnElement.addEventListener("click", () => {
  let requestLoan = parseInt(prompt("Enter an amount you want to loan: "));
  if (outstandingLoan > 0) alert("You can not get two loans! 😋");
  else if (requestLoan > balance * 2) {
    alert("You can not get a loan more than double of your bank balance! 🤗");
  } else {
    balance += requestLoan;
    bankAmountElement.innerHTML = balance;
    outstandingLoan += requestLoan;
    loanAmountElement.innerText = outstandingLoan;
  }
});

// to show the balance and salary

const printBalance = () => {
  salaryAmountElement.innerHTML = salary;
  bankAmountElement.innerHTML = balance;
};

// The repayLoanBtnElement basically checks if the personHasLoan don
repayLoanBtnElement.addEventListener("click", () => {
  let rest = 0;

  if (outstandingLoan == 0) {
    alert("You don't have a loan to pay back...");
  }
  if (salary >= outstandingLoan) {
    rest = salary - outstandingLoan;
    balance += rest;
    salary = 0;
    outstandingLoan -= outstandingLoan;
    rest = 0;
    salaryAmountElement.innerHTML = salary;
    bankAmountElement.innerHTML = balance;
    loanAmountElement.innerHTML = outstandingLoan;
  }
  if (salary < outstandingLoan) {
    rest = outstandingLoan - salary;
    outstandingLoan = rest;
    salary = 0;
    salaryAmountElement.innerHTML = salary;
    loanAmountElement.innerText = outstandingLoan;
  }
});

//the buy button method which checks if your balance is more or the same as the computerTotal which is the selected computers price. Then it basically buys the computer you choose and deducts the amount from your account.
// if you do not have enough amount then it shows you the alert message

buyBtnElement.addEventListener("click", () => {
  const selectedComputer = laptops[laptopsElement.selectedIndex];
  const computerTotal = selectedComputer.price;
  if (balance >= computerTotal) {
    balance = balance - computerTotal;
    bankAmountElement.innerHTML = balance;
    alert(
      "You bought a new computer, check your account. YOU ARE NOW BROKE!! 🤑😭😢"
    );
  } else {
    alert("Get a job mate, your account can't afford this. 🤣😂");
  }
});

laptopsElement.addEventListener("change", chosenLaptopFunction);
